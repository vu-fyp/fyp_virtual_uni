<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Car Palace</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- Le styles -->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

    <link href="assets/css/bootstrap.elemento.min.css" rel="stylesheet">
    <!-- scripts -->
    <script src="assets/js/jquery.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/application.js"></script>
</head>

<body>

    <!-- offcanvas content wrapper -->
    <div class="oc-wrapper">

        <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle oc-toggle" data-target="#oc-navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.php">Car Palace</a>
                </div>

                <!-- offcanvas navbar -->
                <div class="nav-collapse oc-navbar"  id="oc-navbar">
                    <!-- Menu right shadow -->
                    <div class="oc-shadow"></div>
                    <!-- slideback container -->
                    <ul class="nav navbar-nav onhover navbar-right oc-container" >
                        <!-- offcanvas menu CLOSE button -->
                        <li class="text-right"> <em class="fa fa-times fa fa-2x oc-close"></em>
                        </li>
                        <li>
                            <a href="index.php">Home</a>
                        </li>
                        <li>
                            <a href="login.php">Login</a>
                        </li>
                         <li>
                            <a href="signup.php">Sign Up</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse --> </div>
        </nav>

        <div class="container">

            <!-- Carousel
        ================================================== -->
            <div class="carousel slide">
            
                <div class="carousel-inner">
                    <!-- Slide 1 -->
                    <div class="item active">
                        <img src="images/banner.jpg" alt="">
                    </div>
                 </div>  
            </div>
                        <!-- Example row of columns -->
            <hr>
           
           <!-- tabs starts fro here -->
           <div class="container">
           <div class="col-sm-12 col-lg-12">
                            <p class="lead text-muted">Browse Cars By</p>
                            <div class="tabbable">
                                <ul class="nav nav-tabs">
                                    <li class="active">
                                        <a href="#tab-category" data-toggle="tab" aria-expanded="true">Model</a>
                                    </li>
                                    <li class="">
                                        <a href="#tab-model" data-toggle="tab" aria-expanded="false">Category</a>
                                    </li>
                                    <li class="">
                                        <a href="#tab-body" data-toggle="tab" aria-expanded="false">Body Type</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab-category">
                                        <div class="cars-by-category">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="">
                                                        <span class="1000cc-cars">g</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="">
                                                        <span class="1000cc-cars">f</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="">
                                                        <span class="1000cc-cars">d</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="">
                                                        <span class="1000cc-cars">s</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="">
                                                        <span class="1000cc-cars">a</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="">
                                                        <span class="1000cc-cars">h</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="tab-pane" id="tab-model">
                                        <p>
                                            Howdy, I'm in Section 2.Morbi vel nibh et arcu pretium adipiscing. Ut vestibulum est eget justo facilisis ullamcorper.
                                        </p>
                                    </div>
                                    <div class="tab-pane" id="tab-body">
                                        <p>
                                            Howdy, I'm in Section 2.Morbi vel nibh et arcu pretium adipiscing. Ut vestibulum est eget justo facilisis ullamcorper.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <!-- end of tabs -->
                        <hr>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-lg-12">
                    <p class="lead text-muted" style="text-align: center;">Featured Cars</p>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="thumbnail">
                        <a href="#">
                            <img src="Images/bmw.jpg"></a>
                        <div class="caption">
                            <h4>Honda City (2006)</h4>
                            <a href="#" class="btn btn-primary btn-sm">Car Specification</a>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="thumbnail">
                        <img src="Images/bmw.jpg">
                        <div class="caption">
                            <h4>Honda City (2006)</h4>
                            <a href="#" class="btn btn-primary btn-sm">Car Specification</a>
                        </div>
                    </div>
                    <br>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="thumbnail">
                        <img src="Images/bmw.jpg">
                        <div class="caption">
                            <h4>Honda City (2006)</h4>
                            <a href="#" class="btn btn-primary btn-sm">Car Specification</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="thumbnail">
                        <img src="Images/bmw.jpg">
                        <div class="caption">
                            <h4>Honda City (2006)</h4>
                            <a href="#" class="btn btn-primary btn-sm">Car Specification</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
            <div class="row">
                <div class="col-sm-3 col-xs-6">
                    <div class="thumbnail">
                        <a href="#">
                            <img src="Images/bmw.jpg"></a>
                        <div class="caption">
                            <h4>Honda City (2006)</h4>
                            <a href="#" class="btn btn-primary btn-sm">Car Specification</a>
                        </div>
                    </div>
                    <br></div>
                <div class="col-sm-3 col-xs-6">
                    <div class="thumbnail">
                        <img src="Images/bmw.jpg">
                        <div class="caption">
                            <h4>Honda City (2006)</h4>
                            <a href="#" class="btn btn-primary btn-sm">Car Specification</a>
                        </div>
                    </div>
                    <br></div>
                <div class="col-sm-3 col-xs-6">
                    <div class="thumbnail">
                        <img src="Images/bmw.jpg">
                        <div class="caption">
                            <h4>Honda City (2006)</h4>
                            <a href="#" class="btn btn-primary btn-sm">Car Specification</a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3 col-xs-6">
                    <div class="thumbnail">
                        <img src="Images/bmw.jpg">
                        <div class="caption">
                            <h4>Honda City (2006)</h4>
                            <a href="#" class="btn btn-primary btn-sm">Car Specification</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- continer ends here main -->         
    </div>
     <footer class="text-center" style="background: #23292f;">
                <hr>
                <p>
                    <a href="#">
                        <em class="fa fa-chevron-up"></em>
                    </a>
                </p>
                <p>2019 &copy; All rights reserved.</p>
                <br>
            </footer>
    <!-- /ocwrapper -->
<script type="text/javascript">
            $(function() {
                $('#myCarousel').carousel();
            });
      </script>
</body>
</html>