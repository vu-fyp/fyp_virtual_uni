<?php
require('registration.php');
   $msg = '';
   if (isset($_POST['login'])) {
       $email = $_POST['email'];
       $pass = $_POST['password'];

       $query = $user->userLogin($email,$pass);

       if ($query) {
           header('Location: index.php');
       } else{
            echo '<script language="javascript">';
            echo 'alert("Invalid email or password.")';
            echo '</script>';
            // header('Location: login.php');
       }
   }
?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Car Palace</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <!-- Le styles -->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

    <link href="assets/css/bootstrap.elemento.min.css" rel="stylesheet">

</head>

<body>

    <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle oc-toggle" data-target="#oc-navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                   <a class="navbar-brand" href="index.php">Car Palace</a>
                </div>

                <!-- offcanvas navbar -->
                <div class="nav-collapse oc-navbar"  id="oc-navbar">
                    <!-- Menu right shadow -->
                    <div class="oc-shadow"></div>
                    <!-- slideback container -->
                    <ul class="nav navbar-nav onhover navbar-right oc-container" >
                        <!-- offcanvas menu CLOSE button -->
                        <li class="text-right"> <em class="fa fa-times fa fa-2x oc-close"></em>
                        </li>
                        <li>
                           <a href="index.php">Home</a>
                        </li>
                        <li>
                            <a href="login.php">Login</a>
                        </li>
                         <li>
                            <a href="signup.php">Sign Up</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse --> </div>
        </nav>

    <!-- Login Page Tabs -->
    <div class="container">
        <div class="row"><?php echo $msg;?></div>
    <div class="row">
    <div class="col-sm-6 col-lg-12">
                            <div class="tabbable tabs-below">
                                <div class="tab-content">
                                    <div class="tab-pane active" id="tab41">
                                        <div class="row">
                                            <div class="col-sm-6 col-lg-6" data-effect="slide-right">
                            <p class="lead text-muted">Sign in if you already have an account with us</p>
                            <form class="form-horizontal" method="POST">
                                <div class="form-group">
                                    <label for="inputEmail" class="col-lg-2 control-label">Email</label>
                                    <div class="col-lg-10">
                                        <input type="text" class="form-control" name="email" id="inputEmail" placeholder="Email"></div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="col-lg-2 control-label">Password</label>
                                    <div class="col-lg-10">
                                        <input type="password" class="form-control" name='password' id="inputPassword" placeholder="Password"></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox">Remember me</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-2 col-lg-10">
                                        <button type="submit" name="login" class="btn btn-default">Sign in</button>
                                    </div>
                                </div>
                            </form>
                            </br>
                            </br>
                            <p><h3> If you are not Signed Up yet Please <a href="signup.php">Sign Up Now</a></h3></p>
                        </div>
                                            
                                        </div>
                                    </div>
                                    
            <!-- Footer
        ================================================== -->
            <footer class="text-center">
                </br>
</br>
<p>Car Palce 2019 &copy; All rights reserved.</p>
                <br></footer>

        </div>
        <!-- /container -->

        <!-- Le javascript
      ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="assets/js/jquery.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>
    </div>
    <!-- /ocwrapper -->

</body>
</html>
