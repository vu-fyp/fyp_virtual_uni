<?php
require_once  'registration.php';
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



$flag = false;
 if ($_SERVER["REQUEST_METHOD"] == "POST") {
             $fname = $_POST['fname'];
             $lname = $_POST['lname'];
             $email = $_POST['email'];
             $password = $_POST['password'];
            $mobileNo = $_POST['mobile'];
            $cnic = $_POST['cnic'];
            $address = $_POST['address'];

            $result = $user->RegisterUser($fname, $lname, $email, $password, $mobileNo, $cnic, $address);
           
            $flag =$result;
            //exit;
           // header('Location: signup.php');
                // check here if any sol found.

        }

 ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
  <title>Car Palace</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">

    <!-- Le styles -->
    <link href='http://fonts.googleapis.com/css?family=PT+Sans:400,700,400italic' rel='stylesheet' type='text/css'>

    <link href="assets/css/bootstrap.elemento.min.css" rel="stylesheet">

</head>

<body>

    <nav class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle oc-toggle" data-target="#oc-navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                  <a class="navbar-brand" href="index.php">Car Palace</a>
                </div>

                <!-- offcanvas navbar -->
                <div class="nav-collapse oc-navbar"  id="oc-navbar">
                    <!-- Menu right shadow -->
                    <div class="oc-shadow"></div>
                    <!-- slideback container -->
                    <ul class="nav navbar-nav onhover navbar-right oc-container" >
                        <!-- offcanvas menu CLOSE button -->
                        <li class="text-right"> <em class="fa fa-times fa fa-2x oc-close"></em>
                        </li>
                        <li>
                             <a href="index.php">Home</a>
                        </li>
                        <li>
                            <a href="login.php">Login</a>
                        </li>
                         <li>
                            <a href="signup.php">Sign Up</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse --> </div>
        </nav>

    <!-- signup Page  -->
    <div class="container">
        <div class="row"><?php if ($flag){ echo '<div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert">×</button>
                        <strong>Registered successfully!</strong>
                        You have regustered successfully Please go to <a href="login.php">Login Page</a>.
                    </div>'; } ?></div>
  

  <input type="text" name="vuid">
  <button type="submit"> Submit </button>
      
                                        <div class="row">
                                            <div class="col-sm-12 col-lg-6" data-effect="slide-right">
                            <p class="lead text-muted">Register Your Account</p>
                            <form class="form-horizontal" method="POST" action="">
                                <div class="form-group">
                                    <label for="inputFname" class="col-lg-3 control-label">First Name</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="inputFname" name="fname" required="required" placeholder="First Name"></div>
                                </div>
                                <div class="form-group">
                                    <label for="inputLname" class="col-lg-3 control-label">Last Name</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="inputLname" name="lname" required="required" placeholder="Last Name"></div>
                                </div>
                                <div class="form-group">
                                    <label for="inputEmail" class="col-lg-3 control-label">Email</label>
                                    <div class="col-lg-9">
                                        <input type="email" class="form-control" id="inputEmail" name="email" required="required" placeholder="Email"></div>
                                </div>
                                <div class="form-group">
                                    <label for="inputPassword" class="col-lg-3 control-label">Password</label>
                                    <div class="col-lg-9">
                                        <input type="password" class="form-control" id="inputPassword" name="password" required="required" placeholder="Password"></div>
                                </div>
                                <div class="form-group">
                                    <label for="inputNumber" class="col-lg-3 control-label">Mobile Number</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="inputNumber" name="mobile" required="required" data-mask="9999-9999999" placeholder="0300-1234567"></div>
                                </div>
                                <div class="form-group">
                                    <label for="inputCnic" class="col-lg-3 control-label">CNIC</label>
                                    <div class="col-lg-9">
                                        <input type="text" class="form-control" id="inputCnic" name="cnic" required="required" data-mask="99999-9999999-9" placeholder="CNIC Number"></div>
                                </div>
                                <div class="form-group">
                                    <label for="inputAddress" class="col-lg-3 control-label">Address</label>
                                    <div class="col-lg-9">
                                        <textarea class="form-control" id="inputAddress" name="address" required="required" placeholder="Home Address" rows="3"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-lg-offset-4 col-lg-4">
                                        <button type="submit" name="submit" class="btn btn-success">Register</button>
                                    </div>
                                    <div class="form-group">
                                    <div class=" col-lg-4">
                                        <button type="reset" class="btn btn-warning">Reset</button>
                                    </div>
                                </div>
                                </div>
                            </form>
                        </div>
                                            
                                        </div>
                                    </div>
                                </div>
            <!-- Footer
        ================================================== -->
          
          </br>
</br>
  <footer class="text-center">
                <p>Car Palce 2019 &copy; All rights reserved.</p>
                <br></footer>

        </div>
        <!-- /container -->

</body>
</html>
